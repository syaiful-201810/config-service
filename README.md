# Aplikasi Config Service #

Cara deploy :

1. Buat aplikasi di Heroku, misalnya nama aplikasinya `tm2018-config`

2. Enable Dyno Info melalui CLI

        heroku labs:enable runtime-dyno-metadata -a tm2018-config

3. Set konfigurasi profile Spring

        heroku config:set SPRING_PROFILES_ACTIVE=heroku -a tm2018-config

3. Tambahkan remote git Heroku

        git remote add heroku https://git.heroku.com/tm2018-config.git

4. Push ke Heroku

        git push heroku master